import threading
import random


class BaseThread(threading.Thread):

    def __init__(self,count_):

        super().__init__()
        self.count_ = count_

    def run(self):

        try :

            for i in range(self.count_):

                file_name = f"test_file{i+1}.txt"

                range_start = 10**(self.count_-1)
                range_end = (10**self.count_)-1

                random_numbers =  random.randint(range_start, range_end)

                with open(file_name,"w") as file_:

                    file_.write(f"{random_numbers}\n")
                
            print("THREAD SUCCESS")

        except Exception as e:

            print("THREAD FAILED")

if __name__ == "__main__":

    thread_ = BaseThread(5)

    thread_.start()